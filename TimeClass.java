import java.time.format.DateTimeFormatter;
import java.time.*;
import java.util.Scanner;
import java.util.regex.*;

public class TimeClass {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        String date= scan.nextLine();
        Pattern pattern = Pattern.compile("^(0[1-9]|[12]\\d|3[01])[- /.](0[1-9]|1[012])[- /.](\\d{4})$");
        Matcher matcher = pattern.matcher(date);
        if(matcher.find()){
            DateTimeFormatter dateForm = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            LocalDate date1=LocalDate.parse(date,dateForm);
            if(date1.isLeapYear()){
                System.out.println(date1 + " is leap year");
            }
            else{
                System.out.println(date1 + " isn't a leap year");
            }
        }
        else{
            System.out.println("Incorrect date!");
        }
    }
}
